import sys, os, settings
from addressparser import AddressParser

if __name__ == '__main__':
    num_args = len(sys.argv)
    if num_args in [2,3]:
        if num_args == 3 and sys.argv[2] != '-v' and sys.argv[2] != '--v':
            print "Unrecognized argument: " + sys.argv[2]
        parser = AddressParser()
        filename = sys.argv[1]
        parser.check_addresses(filename)
        if num_args == 3 and (sys.argv[2] == '-v' or sys.argv[2] == '--v'):
            parser.print_log()
    elif num_args < 2:
        print "Usage:\npython " + os.path.basename(__file__) + " [input file] [-v]"
        exit()
    elif num_args > 3:
        print "Too many arguments. Include only one file name to process, and optionally the -v flag."
        exit()