# Dependencies
The following Python packages are required:

`pip install googlemaps`

`pip install shapely`

Install the above packages using pip.  For help installing Shapely on a Windows system, see [http://www.lfd.uci.edu/~gohlke/pythonlibs/#shapely](http://www.lfd.uci.edu/~gohlke/pythonlibs/#shapely).

Additionally, an API key authorized for the Google geocoding project is required. See [https://console.developers.google.com/apis/api/geocoding_backend/](https://console.developers.google.com/apis/api/geocoding_backend/). 

# Configuration
To configure this project, rename the file **settings.py.example** to **settings.py** and set the value of **google_maps_api_key** to your API key.

# Usage
```python
python process_address_list.py input.txt
```
input.txt must be formatted like this:
```
(37.5, -122.5), (38.2, -121.6), (37.0, -121.4), (36.6, -121.3)
1706 Forest View Ave Hillsborough CA 94010
```
with the first line as a list of coordinates for a bounding box, and subsequent lines containing valid postal addresses.

## Optional Verbose Mode
For verbose error output, run `process_address_list.py` with the `-v` flag:
```python
python process_address_list.py input.txt -v
```
In verbose mode, errors parsing input or looking up addresses are output to the terminal.

# Unit Testing
To run this project's unit tests, run:
```python
python polygontest.py
```

# Project Description
This Python script processes an input list of coordinates and addresses,
where the first line is a list of coordinates for a bounding box, and subsequent lines containing valid postal addresses,
and prints out the addresses whose coordinates are inside the bounding box.

It uses the [googlemaps](https://pypi.python.org/pypi/googlemaps/) client library to find latitude and longitude for the addresses, and
uses the [Shapely](https://pypi.python.org/pypi/Shapely) package to perform raycasting point-in-polygon test on the resulting latitude and longitude.

## Background and Technical Decisions
* Python was chosen for development speed, integrated unit testing, and the availability of appropriate packages.
* Shapely was selected for its speed and reliability -- a better choice than implementing my own version of a point-in-polygon algorithm.
* The point-in-polygon test that this project uses is Euclidean and does not account for the curvature of the earth.  If the polygon was very large, it's possible that the point-in-polygon test will be inaccurate for points close to the edge of the polygon.
After research, I concluded that most similar implementations also use a Euclidean approach, and I could not find a comprehensive solution.  It is possible that [pyproj](https://pypi.python.org/pypi/pyproj) could be used for this.

## Future development
* For the entry point script (`process_address_list.py`), I considered using a package like [argparse](https://docs.python.org/3/library/argparse.html) or [optparse](https://docs.python.org/2/library/optparse.html) because the logic is simple enough.  If any additional complexity is added, it would be good to use a package to make the code more maintainable/readable.
* For use in large-scale applications, consider storing the latitude and longitude lookup results in a database for future reference.  Refactor latlngfacade.py to query the local database before hitting a third party API.


