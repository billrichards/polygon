from shapely.geometry import Polygon, Point
import settings

class Boundary:
    def create_poly(self,bounds):
        self.poly = Polygon(bounds)
        #self.poly = Polygon(([-80.190262, 25.774252], [-66.118292, 18.466465], [-64.75737, 32.321384]))

    def check_boundary(self,coords):
        p1 = Point(coords[0],coords[1])
        return self.poly.contains(p1)
