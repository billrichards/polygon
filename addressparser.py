from boundary import Boundary
from datetime import datetime
from latlngfacade import LatLngFacade
import os.path

class AddressParser:
    def __init__(self):
        self.polygon = []
        self.addresses = []
        self.addresses_and_coordinates = []
        self.error_log = []
        self.latlng = LatLngFacade()

    def check_addresses(self, filename):
        self.process_file(filename)
        self.test_bounds()

    def process_file(self, filename):
        if(not os.path.isfile(filename)):
            self.error_log.append(["File not found: ",filename])
            return False
        else:
            fh = open(filename, "r")
            for line in fh.readlines():
                self.addresses.append(line.strip())
            fh.close()
            if len(self.addresses) == 0:
                self.error_log.append(['Insufficient data for polygon',self.addresses])
                return False
            else:
                self.polygon = self.parse_polygon(self.addresses[0])
            del self.addresses[0]
            for address in self.addresses:
                self.addresses_and_coordinates.append([address, self.get_latitude_longitude(address)])
            return True

        
    def parse_polygon(self,polygonstring):
        coordinates = []
        for pair in polygonstring.replace(" ","").strip("(").strip(")").split("),("):
            latlng = pair.split(",")
            coordinates.append([float(latlng[0]),float(latlng[1])])
        return coordinates
    
    def get_latitude_longitude(self,address):
        latlng = self.latlng.get_lat_lng_from_address(address)
        if isinstance(latlng, list) and len(latlng) == 2:
            return latlng
        else:
            self.error_log.append(['No latitude longitude found',address])
            return []
    
    def test_bounds(self):
        if len(self.polygon) >= 3:
            boundary = Boundary()
            boundary.create_poly(tuple(self.polygon))
            for info in self.addresses_and_coordinates:
                if len(info) >= 2 and boundary.check_boundary(info[1]):
                    print info[0]
        else:
            self.error_log.append(['Insufficient data for polygon',self.polygon])
                
    def print_log(self):
        for i,j in self.error_log:
            print str(i) + ": " + str(j)