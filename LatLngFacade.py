import googlemaps
import settings

class LatLngFacade:

    def __init__(self):
        self.gmaps = googlemaps.Client(key=settings.google_maps_api_key)
        
    def get_lat_lng_from_address(self, address):
        result = self.google_geocode(address)
        if isinstance(result,list) and len(result) >= 1 and isinstance(result[0],dict) and ('geometry' in result[0]) and ('location' in result[0]['geometry']) and ('lat' in result[0]['geometry']['location']) and ('lng' in result[0]['geometry']['location']):
            data = result[0]['geometry']['location']
            return [data.get('lat'),data.get('lng')]
        else:
            return []
        
    def google_geocode(self, address):
        try:
            result = self.gmaps.geocode(address)
        except:
            result = []
        return result
    