import unittest
from addressparser import AddressParser
from boundary import Boundary
from latlngfacade import LatLngFacade
from shapely.geometry import Polygon
import os.path

'''
boundary.create_poly creates a Polygon
'''
class CreatePolyCreatesPolygon(unittest.TestCase):
    def runTest(self):
        boundary = Boundary()
        boundary.create_poly(tuple([[-80.190262, 25.774252], [-66.118292, 18.466465], [-64.75737, 32.321384]]))
        assert isinstance(boundary.poly, Polygon)
        
'''
addressparser.parse_polygon creates a list with coordinates, given a valid input string like (37.5, -122.5), (38.2, -121.6), (37.0, -121.4), (36.6, -121.3)
'''
class ParsePolygonParsesPolygon(unittest.TestCase):
    def runTest(self):
        addressparser = AddressParser()
        polygon = addressparser.parse_polygon('(37.5, -122.5), (38.2, -121.6), (37.0, -121.4), (36.6, -121.3)')
        assert isinstance(polygon, list)
   
'''
Google Maps service returns results in the expected format
'''   
class GoogleGeocodeApiReturnsResults(unittest.TestCase):
    def runTest(self):
        latlngfacade = LatLngFacade()
        result = latlngfacade.google_geocode('30840 Northwestern Highway, Farmington Hills, MI')
        assert (isinstance(result,list) and len(result) >= 1 and isinstance(result[0],dict) and ('geometry' in result[0]) and ('location' in result[0]['geometry']) and ('lat' in result[0]['geometry']['location']) and ('lng' in result[0]['geometry']['location']))
    
class GetLatLngFromAddressReturnsListWithZeroOrTwoElements(unittest.TestCase):
    def runTest(self):
        latlngfacade = LatLngFacade()
        list1 = latlngfacade.get_lat_lng_from_address('~~~~~~~!$%!#$^this is not a real address and it should return an empty list')
        assert (isinstance(list1, list) and len(list1) == 0)
        list1 = latlngfacade.get_lat_lng_from_address('30840 Northwestern Highway, Farmington Hills, MI')
        assert (isinstance(list1, list) and len(list1) == 2)   
        
class GetLatitudeLongitudeReturnsListWithZeroOrTwoElements(unittest.TestCase):
    def runTest(self):
        addressparser = AddressParser()
        list1 = addressparser.get_latitude_longitude('~~~~~~~!$%!#$^this is not a real address and it should return an empty list')
        assert (isinstance(list1, list) and len(list1) == 0)
        list1 = addressparser.get_latitude_longitude('30840 Northwestern Highway, Farmington Hills, MI')
        assert (isinstance(list1, list) and len(list1) == 2)
    
class BoundaryCorrectlyDetected(unittest.TestCase):
    def runTest(self):
        boundary = Boundary()
        boundary.create_poly(tuple([[37.5, -122.5], [38.2, -121.6], [37.0, -121.4], [36.6, -121.3]])) # Bay Area Polygon
        assert True == boundary.check_boundary([37.484116,-122.148244]) # Facebook HQ
        assert False == boundary.check_boundary([42.513649,-83.330900]) # Service.com HQ
     
'''
addressparse.process_file returns True for valid files and False for non-existent or invalid files
'''
class ProcessFileProcessesFile(unittest.TestCase):
    def runTest(self):
        addressparser = AddressParser()
        filename = 'mytestfile.txt'
        i = 0
        while(os.path.isfile(filename)):
            filename = filename + str(i)
            i = i + 1
        fh = open(filename,'w+')
        fh.write('(37.5, -122.5), (38.2, -121.6), (37.0, -121.4), (36.6, -121.3)')
        fh.close()
        assert True == addressparser.process_file(filename)
        os.remove(filename)
        while(os.path.isfile(filename)):
            filename = filename + str(i)
            i = i + 1
        open(filename,'w+')
        assert False == addressparser.process_file(filename) # empty file
        os.remove(filename)
        while(os.path.isfile(filename)):
            filename = filename + str(i)
            i = i + 1
        assert False == addressparser.process_file(filename) # file does not exist       

if __name__ == '__main__':
    unittest.main() 
